#include <iostream>
#include <sys/resource.h>

//prints all limits
void printRLimits(){
    struct rlimit rl;

    printf("\nstack: \n", RLIMIT_STACK);
    getrlimit(RLIMIT_STACK, &rl);
    printf("soft-limit: %lld\n", (long long int) rl.rlim_cur);
    printf("hard-limit: %lld\n", (long long int) rl.rlim_max);

    printf("\ndata: \n", RLIMIT_DATA);
    getrlimit(RLIMIT_DATA, &rl);
    printf("soft-limit: %lld\n", (long long int) rl.rlim_cur);
    printf("hard-limit: %lld\n", (long long int) rl.rlim_max);
    if(RLIM_INFINITY == rl.rlim_max){
        std::cout << "Heap is unlimited." << std::endl;
    }
}

int depht = 0;
void function(){
    depht++;
    std::cout << std::endl << "exec: function()";
    printf("\nfunction address main: %p\n", function);
    //init variables on heap
    int * heap_1 = new int(1);
    int * heap_2 = new int(2);
    int * heap_3 = new int(4);

    //init variable on stack
    int stack_1 = 1;
    int stack_2 = 2;
    int stack_3 = 1000;

    printf("\nheap 1: %p\n", heap_1);
    printf("heap 2: %p\n", heap_2);
    printf("heap 3: %p\n", heap_3);

    printf("\nstack 1: %p\n", &stack_1);
    printf("stack 2: %p\n", &stack_2);
    printf("stack 3: %p\n", &stack_3);

    delete heap_1;
    delete heap_2;
    delete heap_3;
    if(depht <= 1)
        function();
}

int main()
{
    std::cout << "exec: main()" << std::endl;

    printf("\nfunction address main: %p\n", main);

    printRLimits();

    //init variables on heap
    int * heap_1 = new int(1);
    int * heap_2 = new int(2);
    int * heap_3 = new int(4);

    //init variable on stack
    int stack_1 = 1;
    int stack_2 = 2;
    int stack_3 = 1000;

    printf("\nheap 1: %p\n", heap_1);
    printf("heap 2: %p\n", heap_2);
    printf("heap 3: %p\n", heap_3);

    printf("\nstack 1: %p\n", &stack_1);
    printf("stack 2: %p\n", &stack_2);
    printf("stack 3: %p\n", &stack_3);

    delete heap_1;
    delete heap_2;
    delete heap_3;

    function();

    return 0;
}
