#include <iostream>
#include <string>
#include <list>
#include <unistd.h>
#include <vector>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>

using namespace std;

/*
 * reads until new line operator (Enter)
 */
string getInput(){
    string in{};
    while(1){
        char c = fgetc(stdin);
        if(c == '\n'){
            return in;
        }else{
            in += c;
        }
    }
}

int main()
{
    /*
     * list of child process ids (unused)
     */
    list<pid_t> childList;
    /*
     * boolean to terminate programm
     */
    bool b_exit(false);

    do{
        printf("myshell> ");
        /*
         * read the input and determinate if prozess should be run in fg or bg
         */
        string readC = getInput();
        int len = readC.length();
        bool backg(false);

        if(readC[len-1] == '&'){
            backg = true;
            readC[len-1] = '\0';
        }else{
            readC += '\0';
            ++len;
        }

        /*
         * skip to next usable char
         */
        int i = 0;
        for(; readC[i] == ' '; i++);
        /*
        * process user input and save to seperate strings
        */
        string sProg{};
        while(readC[i] != ' ' && readC[i] != '\0'){
            sProg+= readC[i];
            ++i;
        }
        ++i;
        string sArgs{};
        while(readC[i] != '\0'){
            sArgs+= readC[i];
            ++i;
        }
        sArgs+= '\0';
        /*
        * if logout was promted, ask for confirm
        * else continue with the programm
        */
        if(sProg == "logout" || sProg == "exit"){
            cout << "logout? (y/n)? ";
            string confirm;
            cin >> confirm;
            if(confirm == "y" || confirm == "Y")
                b_exit = true;
        }else{

            pid_t pid = 0, wait_pid = 0;

            /*
             * horrible pointer casting madness
             * because strings are easier to edit than char*
             */
            char* prog = const_cast<char*>(sProg.c_str());
            vector<char*> vArgs;
            vector<string> vSArgs;

            int i = 0;
            string arg = "";
            while(sArgs[i] != '\0'){
                if(sArgs[i] != ' '){
                    arg+= sArgs[i];
                }else{
                    vSArgs.push_back(arg);
                    arg = "";
                }
                i++;
            }
            if(arg != " " && arg != "")
                vSArgs.push_back(arg);

            /*
             * even more horrible pointer casting madness
             */
            for(size_t i = 0; i < vSArgs.size(); i++){
                vArgs.push_back(const_cast<char*>(vSArgs.at(i).c_str()));
            }

            /*
             * copy the arguments parsed to char* from vector list
             * into a new char* array which can be passed to the execvp() systemcall
             *
             * since execvp expects the programm as first argument and NULL as last
             * insert both values aswell as the paramenters parsed from user-input
             * into the array
             *
             * and yes, even more horrible pointer casting madness
            */
            size_t argc = vArgs.size();
            char* args[argc+2];
            args[0] = const_cast<char*>(sProg.c_str());;
            for(size_t i = 1; i < argc+1; i++){
                args[i] = vArgs[i-1];
            }
            args[argc+1] = NULL;

            /*
             * fork the process
             * decide if the process should be run in fg or bg
             *
             * if child returns unsuccessfull run, exit
             * parent waits for exit if run in fg
             */
            pid = fork();
            if(backg){
                if(pid == 0){
                    if(execvp(prog, args)<0){
                        cout << "err" << endl;
                        return 0;
                    }
                }else{
                    childList.push_back(pid);
                }
            }else{
                int status;
                if(pid == 0){
                    if(execvp(prog, args)<0){
                        cout << "err" << endl;
                        return 0;
                    }
                }else{
                    childList.push_back(pid);
                    /*
                     * wait for the process to finish
                     */
                    do{
                        wait_pid = waitpid(pid, &status, WUNTRACED);
                    }while(!WIFEXITED(status) && !WIFSIGNALED(status));
                }
            }
        }
    }while(!b_exit);

    return 0;
}
