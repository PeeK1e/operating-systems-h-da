#include <iostream>
#include <string>
#include <list>
#include <unistd.h>
#include <vector>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <map>

#define CREATE_ERROR "Error creating"
#define PROZESS_STOPPED "stopped"
#define PROZESS_EXITED "exited"
#define PROZESS_FINISHED "finished"


using namespace std;

/*
 * list of child process ids (unused)
 */
vector<pid_t> childPid;
vector<pid_t> waitChild;
pid_t childInFg;

/*
 * reads until new line operator (Enter)
 */
string getInput(){
    string in{};
    while(1){
        char c = fgetc(stdin);
        switch(c){
        case '\n':
            return in;
            break;
        default:
            in += c;
            break;
        }
    }
}

/*
 * will be called when a child changes state
 * if the pid is bigger than 0, it means the prozess was executed in background
 * and needs to be deleted from the global childPid list
 */
void childHandler(int sig){
    int status;
    pid_t pid = waitpid(-1, &status, WNOHANG);
    if(pid > 0 && WIFEXITED(status)){
        cout << "[" << pid << "]: " << PROZESS_EXITED << endl;
        for(unsigned long i = 0; i < childPid.size(); i++)
            if(childPid.at(i) == pid){
                childPid.erase(childPid.begin()+i);
                return;
            }
    }
}

/*
 * workaround for a bug in childHandler
 * to not duplicate pid entries
 * could be fixed by using a map or other map-like container
 * for now this will do
 */
void insertPidIfNotExists(pid_t pid){
    for(unsigned long i = 0; i < childPid.size(); i++){
        if(childPid.at(i) == pid){
            return;
        }
    }
    childPid.push_back(pid);
}

/*
 * Is called when CTRL + Z is pressed
 */
void sigstopHandler(int sig){
    if(childInFg > -1){
        cout << endl << sig << " received" << endl;
        waitChild.push_back(childInFg);
        insertPidIfNotExists(childInFg);
        cout << "[" << childInFg << "]: " << PROZESS_STOPPED << endl;
        kill(childInFg, SIGTSTP);
    }
}

/*
 * take the given pid and wait for a statechange of the prozess
 *
 * sets the current executing pid globally
 *
 * after statechange, it changes the globally assigned pid to -1
 */
void doInFg(pid_t pid){
    int status{};
    setpgid(pid, pid);
    childInFg = pid;
    do{
        waitpid(pid, &status, WUNTRACED);
    }while(!WIFSIGNALED(status) && !WIFEXITED(status) && !WIFSTOPPED(status));
    /*
     * check if prozess needs to be removed
     */
    if(WIFEXITED(status)){
        for(unsigned long i = 0; i < childPid.size(); i++)
            if(childPid.at(i) == pid){
                childPid.erase(childPid.begin()+i);
                break;
            }
    }
    childInFg = -1;
}

/*
 * checks if the given pid is waiting for execution
 * if found deletes the pid from the list and returns it to caller
 * else return -1
 */
int retireveWaitingProcessId(pid_t pid){
    for(unsigned long i = 0; i < waitChild.size(); i++){
        if(waitChild.at(i) == pid){
            waitChild.erase(waitChild.begin()+i);
            return pid;
        }
    }
    return -1;
}

/*
 * creates a child with given params
 */
void createChild(char* prog, char* args[]){
    setpgid(0, getpid());
    if(execvp(prog, args)<0){
        cout << CREATE_ERROR << endl;
        exit(0);
    }
}

int main()
{
    /*
     * boolean to terminate programm
     */
    bool b_exit(false);

    /*
     * installs SIGNAL handlers
     */
    signal(SIGCHLD, childHandler);
    signal(SIGTSTP, sigstopHandler);

    do{
        cout << "myshell> ";
        /*
         * read the input
         */
        string readC = getInput();
        int len = readC.length();
        bool backg(false);

        /*
         * if '&' was given in the parameters, set to run in BG
         */
        if(readC[len-1] == '&'){
            backg = true;
            readC[len-1] = '\0';
        }else{
            readC += '\0';
            ++len;
        }

        /*
         * skip to next usable char
         */
        int i = 0;
        for(; readC[i] == ' '; i++);

        /*
        * process user input and save to seperate strings
        *
        * sProg contains the parsed command to execute
        *
        * sArgs contains the arguments to pass
        */
        string sProg{};
        while(readC[i] != ' ' && readC[i] != '\0'){
            sProg+= readC[i];
            ++i;
        }
        ++i;
        string sArgs{};
        while(readC[i] != '\0'){
            sArgs+= readC[i];
            ++i;
        }
        /*
         * terminate arguments with endl char
         * for easier parsing
         */
        sArgs+= '\0';

        /*
         * horrible pointer casting madness
         * because strings are easier to edit than char*
         *
         * @prog contains the program
         * @vArgs a vector of char* which holds the parsed arguments
         * @vSArgs a vector of string which holds the parsed arguments
         */
        char* prog = const_cast<char*>(sProg.c_str());
        vector<char*> vArgs;
        vector<string> vSArgs;

        /*
         * parsing the arguments
         * inserts them into vArgs
         */
        i = 0;
        string arg = "";
        while(sArgs[i] != '\0'){
            if(sArgs[i] != ' '){
                arg+= sArgs[i];
            }else{
                vSArgs.push_back(arg);
                arg = "";
            }
            i++;
        }
        if(arg != " " && arg != "")
            vSArgs.push_back(arg);

        /*
         * casting from strings into char*
         */
        for(size_t i = 0; i < vSArgs.size(); i++){
            vArgs.push_back(const_cast<char*>(vSArgs.at(i).c_str()));
        }

        /*
         * copy the arguments parsed to char* from vector list
         * into a new char* array which can be passed to the execvp() systemcall
         *
         * since execvp expects the programm as first argument and NULL as last
         * insert both values aswell as the paramenters parsed from user-input
         * into the array
         */
        size_t argc = vArgs.size();
        char* args[argc+2];
        args[0] = const_cast<char*>(sProg.c_str());;
        for(size_t i = 1; i < argc+1; i++){
            args[i] = vArgs[i-1];
        }
        args[argc+1] = NULL;

        /*
        * if logout was promted, ask for confirm
        * on fg; retrieve the pid and continue in fg
        * on bg; retrieve the pid and continue in bg
        * else spawn the prozess
        */

        if(sProg == "logout" || sProg == "exit"){

            if(childPid.size() > 0){
                cout << "there are still processes running: " << endl;
                for(unsigned long i = 0; i < childPid.size(); i++)
                    cout << i <<" : " << childPid.at(i) << endl;
            }else{

                cout << "logout? (y/n)? ";
                string confirm;
                confirm = getInput();

                if(confirm == "y" || confirm == "Y"){
                    b_exit = true;
                }
            }
        }else if(sProg == "fg"){
            /*
             * executes the before stopped command in fg
             */
            if(vSArgs.size() > 0){
                pid_t pid = retireveWaitingProcessId(stoi(vSArgs.at(0)));
                if(pid != -1){
                    kill(pid, SIGCONT);
                    doInFg(pid);
                }
            }else{
                cout << "specify the process!" << endl;
            }
        }else if(sProg == "bg"){
            /*
             * executes the before stopped command in bg
             */
            if(vSArgs.size() > 0){
                pid_t pid = retireveWaitingProcessId(stoi(vSArgs.at(0)));
                if(pid != -1){
                    kill(pid, SIGCONT);
                }
            }else{
                cout << "specify the process!" << endl;
            }
        }else{
            /*
             * fork the process
             * decide if the process should be run in fg or bg
             *
             * if the child process creation is unsuccessfull return
             * else parent handles further execution
             */
           pid_t pid = fork();
            if(backg){
                if(pid == 0){
                    //child does this:
                    createChild(prog, args);
                }else{
                    //parent does this:
                    setpgid(pid, pid);
                    childPid.push_back(pid);
                }
            }else{
                if(pid == 0){
                    //child does this:
                    createChild(prog, args);
                }else{
                    //parent does this:
                    doInFg(pid);
                }
            }
        }
    }while(!b_exit);

    return 0;
}
