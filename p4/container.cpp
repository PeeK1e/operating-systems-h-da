#include "container.h"

Container::Container(int maxContent, std::string content, long timeInS)
    : m_maxContent(maxContent),
      m_currentContent(m_maxContent),
      m_content(content)

{
    m_timePerStep = timeInS/4.0;
}

int Container::currentContent() const
{
    return m_currentContent;
}

void Container::take()
{

    if(!isEmpty())
        m_currentContent--;

    std::cout << m_content << ", verbleibend: " << (m_currentContent) << std::endl;

    std::this_thread::sleep_for(std::chrono::microseconds(m_timePerStep*1000000));

}

void Container::fill(int ammount)
{
    while(!isFull()){

        std::this_thread::sleep_for(std::chrono::seconds(1));

        if(m_currentContent+ammount > m_maxContent){
            m_currentContent = m_maxContent;
        }else{
            m_currentContent += ammount;
        }
    }
}

bool Container::isFull()
{
    if(m_currentContent == m_maxContent)
        return true;
    return false;
}

bool Container::isEmpty()
{
    if(m_currentContent < 1)
        return true;
    return false;
}

void Container::grabContainer()
{
    m_accesMutex.lock();
}

void Container::releaseContainer()
{
    m_accesMutex.unlock();
}

std::string Container::contentName() const
{
    return m_content;
}





