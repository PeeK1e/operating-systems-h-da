#include "mitarbeiter.h"

Mitarbeiter::Mitarbeiter(Container *fleisch,
                         Container *sauce,
                         Container *salat,
                         Container *brot,
                         Refiller *refiller,
                         int id) :
    m_fleisch(fleisch),
    m_sauce(sauce),
    m_salat(salat),
    m_brot(brot),
    m_refiller(refiller)
{
    sem_init(&m_sem, 0, id);
}

void Mitarbeiter::takeOrder(Kunde *customer)
{
    //sem_t *customerSem = customer->getCustomerSem();

    std::cout << "Ok!" << std::endl;

    for(int i = 0; i < customer->getOrderAmmount(); i++){
        takeAnIngredient(m_brot);
        takeAnIngredient(m_fleisch);
        takeAnIngredient(m_salat);
        takeAnIngredient(m_sauce);
    }

    std::cout << "Bestellung fertig" << std::endl;

    //sem_post(customerSem);
    customer->takeOrder();

    m_isFree = true;
}

bool Mitarbeiter::isFree()
{
    return m_isFree;
}

void Mitarbeiter::takeAnIngredient(Container *container)
{
    container->grabContainer();

    if(!container->isEmpty()){
        std::cout << "Hole zutat: ";
        container->take();
    }else{
        m_refiller->fillContainer(container, m_sem);
        sem_wait(&m_sem);
        std::cout << "Hole zutat: ";
        container->take();
    }

    container->releaseContainer();
}

sem_t Mitarbeiter::getSem() const
{
    return m_sem;
}

void Mitarbeiter::serverCustomer()
{
    m_isFree = false;
}

Mitarbeiter::~Mitarbeiter()
{
    m_brot = nullptr;
    m_fleisch = nullptr;
    m_refiller = nullptr;
    m_salat = nullptr;
    m_sauce = nullptr;
}

