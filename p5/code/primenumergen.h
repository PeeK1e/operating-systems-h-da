#ifndef PRIMENUMERGEN_H
#define PRIMENUMERGEN_H

#include <iostream>
#include <vector>

class PrimeNumerGen
{
public:
    PrimeNumerGen(int begin = 0, int end = 999999);
    ~PrimeNumerGen();
    void start();
    void abort();
    bool isDone() const;
private:
    bool isPrime(int n);
    int m_begin;
    int m_end;
    bool m_abort;
    bool m_isDone;
    std::vector<int *> calculated;
};

#endif // PRIMENUMERGEN_H
