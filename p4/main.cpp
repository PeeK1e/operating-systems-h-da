#include <iostream>
#include <thread>
#include <stdio.h>
#include <semaphore.h>
#include <queue>
#include "container.h"
#include "refiller.h"
#include "kunde.h"
#include "mitarbeiter.h"
#include <unistd.h>
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <chrono>

std::queue<Kunde*> customerQueue;
std::vector<Mitarbeiter*> employeeList;

std::vector<Kunde*> customerDoneList;

long TIME_TO_FINISH;
int REFILL_RATE;

Mitarbeiter* getFreeEmployee(){
    for(size_t i = 0; i < employeeList.size(); i++){
        if(employeeList.at(i)->isFree()){
            employeeList.at(i)->serverCustomer();
            return employeeList.at(i);
        }
    }
    return nullptr;
}

int main(int argc, char** argv)
{
    int totalDoener{};
    srand (time(NULL));
    int workerAmmount;
    int containerSize;
    int customerAmmount;

    if(argc == 6){
        workerAmmount = atoi(argv[1]); //atoi(argv[1])
        containerSize = atoi(argv[2]); //argv[2]
        REFILL_RATE = atoi(argv[3]); //atoi(argv[3])
        customerAmmount = atoi(argv[4]); //argv[4]
        TIME_TO_FINISH = atoi(argv[5]); //atoi(argv[5])
    }else{
        workerAmmount = 3; //atoi(argv[1])
        containerSize = 6; //argv[2]
        REFILL_RATE = 2; //atoi(argv[3])
        customerAmmount = 5; //argv[4]
        TIME_TO_FINISH = 4; //atoi(argv[5])
    }

    Refiller *refiller = new Refiller(REFILL_RATE);

    Container *contFleisch = new Container(containerSize, "Fleisch", TIME_TO_FINISH);
    Container *contSalat = new Container(containerSize, "Salat", TIME_TO_FINISH);
    Container *contBrot = new Container(containerSize, "Brot", TIME_TO_FINISH);
    Container *contSauce = new Container(containerSize, "Sauce", TIME_TO_FINISH);

    for(auto i = 0; i < customerAmmount;i++){
        customerQueue.push(new Kunde(static_cast<int>(rand()%11), i));
        //customerQueue.push(new Kunde(1, i));
    }

    for(auto i = 0; i < workerAmmount;i++){
        employeeList.push_back(new Mitarbeiter(contFleisch, contSauce, contSalat, contBrot, refiller, i));
    }

    std::cout << "Es gibt " << employeeList.size() << " Mitarbeiter" << std::endl;

    auto start = std::chrono::high_resolution_clock::now();

    while(customerQueue.size() > 0){

        Mitarbeiter *server;

        do{
            server = getFreeEmployee();
        }while (server == nullptr);

        Kunde *customer = customerQueue.front();

        std::cout << "Bediene Kunden " << customer->getCustomerId() << std::endl;

        totalDoener += customer->getOrderAmmount();

        std::thread customerThread(&Kunde::waitForOrder, customer);
        std::thread employeeThread(&Mitarbeiter::takeOrder, server, customer);

        customerThread.detach();
        employeeThread.detach();

        customerDoneList.push_back(customer);
        customerQueue.pop();
    }

    bool allWorkersDone{false};
    do{
        allWorkersDone = true;
        for(auto e: employeeList){
            if(!e->isFree()){
                allWorkersDone = false;
            }
        }
    }while(!allWorkersDone);

    auto stop = std::chrono::high_resolution_clock::now();

    auto duration = duration_cast<std::chrono::microseconds>(stop - start);

    std::cout << (totalDoener ) << " Döner hergestellt" << std::endl;
    std::cout << (totalDoener / ((duration.count()/10000000.0)*1.0)) << " Döner pro Sekunde" << std::endl;
    std::cout << ((duration.count()/10000000.0)*1.0) << " Sekunden" << std::endl;

    delete contBrot;
    delete contFleisch;
    delete contSalat;
    delete contSauce;

    delete refiller;

    for(auto e: employeeList)
        delete e;

    for(auto e: customerDoneList)
        delete e;

    return 0;
}
