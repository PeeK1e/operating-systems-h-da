#ifndef CONTAINER_H
#define CONTAINER_H
#include <string>
#include <mutex>
#include <thread>
#include <chrono>
//#include <unistd.h>
#include <iostream>

class Container
{
public:
    /*
     * constructor to set container for ingredient
     */
    Container(int maxContent = 10, std::string content = "empty", long timeInS = 1);
    /*
     * returns current content in container
     */
    int currentContent() const;
    /*
     * gets one Unit of the ingredient
     */
    void take();
    /*
     * locks the container and fills up
     */
    void fill(int ammountPerSecond);
    /*
     * returns if container is full
     */
    bool isFull();
    /*
     * returns if container is empty
     */
    bool isEmpty();
    /*
     * locks container, no one can use recource now
     */
    void grabContainer();

    /*
     * unlocks container
     */
    void releaseContainer();
    std::string contentName() const;

private:
    int m_maxContent;
    int m_currentContent;
    std::string m_content;
    std::mutex m_accesMutex;
    long m_timePerStep;
};

#endif // CONTAINER_H
