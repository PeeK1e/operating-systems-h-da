#ifndef KUNDE_H
#define KUNDE_H
#include <semaphore.h>
#include <iostream>
#include <unistd.h>

class Kunde
{
public:
    Kunde(int anzDoener, int id);

    void waitForOrder();
    int getOrderAmmount() const;
    sem_t *getCustomerSem();
    void takeOrder();
    int getCustomerId() const;

private:
    int m_nbrDoener;
    sem_t m_customerSem;
    int m_customerId;
    bool m_isWaiting{true};
};

#endif // KUNDE_H
