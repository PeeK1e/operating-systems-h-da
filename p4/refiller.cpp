#include "refiller.h"

Refiller::Refiller(int rate)
    : m_fillRate(rate)
{

}

void Refiller::fillContainer(Container *container, sem_t &semaphore)
{
    m_mutex.lock();

    std::cout << "Fülle " << container->contentName() << " auf..." << std::endl;
    container->fill(m_fillRate);

    m_mutex.unlock();

    sem_post(&semaphore);
}
