#include "kunde.h"

Kunde::Kunde(int anzDoener, int id)
    : m_nbrDoener(anzDoener),
      m_customerId(id+1)
{
    sem_init(&m_customerSem, 0, m_customerId);
}

void Kunde::waitForOrder()
{
    //sem_wait(&m_customerSem);
    while(m_isWaiting);

    std::cout << m_customerId << " hat Döner erhalten" << std::endl;
}

int Kunde::getOrderAmmount() const
{
    return m_nbrDoener;
}

sem_t *Kunde::getCustomerSem()
{
    return &m_customerSem;
}

void Kunde::takeOrder()
{
    //sem_post(&m_customerSem);
    m_isWaiting = false;
}

int Kunde::getCustomerId() const
{
    return m_customerId;
}
