#ifndef MITARBEITER_H
#define MITARBEITER_H
#include "kunde.h"
#include "container.h"
#include "refiller.h"
#include <thread>

class Mitarbeiter
{
public:
    Mitarbeiter(Container *fleisch, Container *sauce, Container *salat, Container *brot, Refiller *refiller, int id);
    void takeOrder(Kunde* customer);
    bool isFree();
    sem_t getSem() const;
    void serverCustomer();
    ~Mitarbeiter();
private:
    void takeAnIngredient(Container *container);
    Container *m_fleisch;
    Container *m_sauce;
    Container *m_salat;
    Container *m_brot;
    Refiller *m_refiller;
    bool m_isFree{true};
    sem_t m_sem;
};

#endif // MITARBEITER_H
