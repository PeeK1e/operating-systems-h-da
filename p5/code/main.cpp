#include <iostream>
#include <thread>
#include <primenumergen.h>
#include <vector>
#include <signal.h>
#include <unistd.h>

using namespace std;
vector<PrimeNumerGen*> pg;
vector<std::thread*> t;

void clearMem(int sig){
    for(auto e: pg){
        e->abort();
        while (!e->isDone());
    }

    for(auto e: t)
        delete e;

    for(auto e: pg)
        delete e;
}

int main()
{
    //avoid memory leak by handling the interrup/kill/term signal
    signal(SIGINT, clearMem);
    signal(SIGKILL, clearMem);
    signal(SIGTERM, clearMem);

    for(int i = 0; i < 16; i++)
        pg.push_back(new PrimeNumerGen(1, 99999));

    for(int i = 0; i < 16; i++)
        t.push_back(new thread(&PrimeNumerGen::start, pg.at(i)));

    for(int i = 0; i < 16; i++)
        t.at(i)->join();

    clearMem(0);

    return 0;
}
