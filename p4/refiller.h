#ifndef REFILLER_H
#define REFILLER_H
#include "container.h"
#include "semaphore.h"
#include <mutex>

class Refiller
{
public:
    Refiller(int rate);
    void fillContainer(Container *container, sem_t &semaphore);
private:
    int m_fillRate;
    std::mutex m_mutex;
};

#endif // REFILLER_H
