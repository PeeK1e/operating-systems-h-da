#include "primenumergen.h"

PrimeNumerGen::PrimeNumerGen(int begin, int end) :
    m_begin(begin),
    m_end(end),
    m_abort(false),
    m_isDone(false)
{
}

PrimeNumerGen::~PrimeNumerGen()
{
    for(auto e: calculated)
        delete e;
}

void PrimeNumerGen::start()
{
    for(int i = m_begin; i <= m_end; i++)
        for(int ii = m_begin; ii <= m_end; ii++){
            if(m_abort){
                m_isDone = true;
                exit(0);
            }
            if(isPrime(i) && isPrime(ii)){
                //write 10 at once to fill up RAM faster
                calculated.push_back(new int(i * ii));
                calculated.push_back(new int(i * ii));
                calculated.push_back(new int(i * ii));
                calculated.push_back(new int(i * ii));
                calculated.push_back(new int(i * ii));
                calculated.push_back(new int(i * ii));
                calculated.push_back(new int(i * ii));
                calculated.push_back(new int(i * ii));
                calculated.push_back(new int(i * ii));
                calculated.push_back(new int(i * ii));
            }
        }
    m_isDone = true;
}

void PrimeNumerGen::abort()
{
    m_abort = true;
}

bool PrimeNumerGen::isDone() const
{
    return m_isDone;
}

bool PrimeNumerGen::isPrime(int n)
{
    if (n <= 1)
        return false;
    // Check from 2 to n-1
    for (int i = 2; i < n; i++)
        if (n % i == 0)
            return false;

    return true;
}
