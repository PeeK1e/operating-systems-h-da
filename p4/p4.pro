TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
LIBS += -lpthread

SOURCES += \
        container.cpp \
        kunde.cpp \
        main.cpp \
        mitarbeiter.cpp \
        refiller.cpp

HEADERS += \
    container.h \
    kunde.h \
    mitarbeiter.h \
    refiller.h
